﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Circle : MonoBehaviour
{
    public float radius;

    private float height2;
    private float width2;

    private float rangeBetweenGo;

    public Circle nextCircle;

    public GameObject[] goInCircle;
    
    void Start()
    {
        goInCircle = new GameObject[transform.childCount];
        
        for (int i = 0; i < transform.childCount; i++)
        {
            goInCircle[i] = transform.GetChild(i).gameObject;
        }
        
        
        /*width2 = Screen.width * radius;
        height2 = Screen.width * radius;*/
        
        float perfectDistanceBetweenGo = 2 * Mathf.PI / goInCircle.Length;

        rangeBetweenGo = perfectDistanceBetweenGo;

        float diff = 0;

        foreach (var go in goInCircle)
        {
            float x = Mathf.Cos(diff) * radius * 2000f;
            float y = Mathf.Sin(diff) * radius * 2000f;

            go.GetComponent<RectTransform>().anchoredPosition = new Vector2(x, y);
            go.GetComponent<PassivePoint>().LinkPoints();
            
            diff += rangeBetweenGo;
        }
        
        //RandomLinksBetweenCircles();
    }
    
    void Update()
    {
       
    }

    public void RandomLinksBetweenCircles()
    {
        if (nextCircle)
        {
            for (int i = 0; i < nextCircle.goInCircle.Length; i++)
            {
                PassivePoint currentNode = nextCircle.goInCircle[i].GetComponent<PassivePoint>();

                float index = i * 1.3f + Random.Range(-1, 1);

                if ((int) index <= goInCircle.Length && (int) index >= 0)
                {
                    currentNode.pointsLinkedWith.Add(goInCircle[(int) index].GetComponent<PassivePoint>());
                    currentNode.LinkPoints();
                }
            }
        }
    }
}
