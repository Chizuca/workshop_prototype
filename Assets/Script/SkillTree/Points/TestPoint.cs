﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestPoint : PassivePoint
{
    public override void Awake()
    {
        base.Awake();
    }
    public override void Effect()
    {
        Debug.Log("Test Effect");
    }
}
