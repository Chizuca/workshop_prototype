﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GenericNode : PassivePoint
{
   public UnityEvent nodeEventTaken;
   
   public override void Effect()
   {
      nodeEventTaken?.Invoke();
   }
}
