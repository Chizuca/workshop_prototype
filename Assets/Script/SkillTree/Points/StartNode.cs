﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class StartNode : PassivePoint
{
    public UnityEvent nodeEventTaken;
    
    void Start()
    {
        if (reachable)
        {
            GetComponent<Image>().color = new Color(0.5f, 0.5f, 0);
        }
    }

    public override bool CanGetPoint()
    {
        if (reachable)
        {
            return true;
        }
        else
        {
            if (reached)
            {
                //Debug.Log("you already have this point");
                return false;
            }

            CheckIfPointIsReachableWithPassing();

            if (!reachable)
            {
                Debug.Log("Point Not reachable");
                return false;
            }
        
            if (passivPointCost > skillTreeLinkedTo.passivPointsLeft)
            {
                Debug.Log("Not enough passive points");
                return false;
            }

            return true;
        }
    }

    public override void Effect()
    {
        nodeEventTaken?.Invoke();
    }
}
