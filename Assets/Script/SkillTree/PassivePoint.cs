﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PassivePoint : MonoBehaviour
{
    public Canvas canvas;
    
    public SkillTree skillTreeLinkedTo;
    
    public int passivPointCost;
    protected int passivPointSpentInThisPoint;
    
    private Button myButton;

    public bool reached;

    public bool reachable;

    public List<PassivePoint> pointsLinkedWith = new List<PassivePoint>();
    
    [TextArea(3, 10)] public string effectDescription;

    private bool _highLighted;

    public virtual void Awake()
    {
        skillTreeLinkedTo = transform.GetComponentInParent<SkillTree>();
        
        myButton = GetComponent<Button>();
        
        SetToSpendMode();
        
        skillTreeLinkedTo.setToSpendMode += SetToSpendMode;

        skillTreeLinkedTo.setToRemoveMode += SetToRemoveMode;
        
        LinkPoints();

        transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = gameObject.name;
    }
    
    public void Update()
    {
        if (_highLighted)
        {
            skillTreeLinkedTo.tipsContainer.GetComponent<RectTransform>().transform.position = 
                new Vector3(Screen.width * skillTreeLinkedTo.tipsPosOnScreenWidth,
                    Screen.height * skillTreeLinkedTo.tipsPosOnScreenHeight, skillTreeLinkedTo.tipsContainer.GetComponent<RectTransform>().transform.position.z);
        }
    }

    public void LinkPoints()
    {
        //Debug.Log("Link Node with Line");
        
        if (pointsLinkedWith.Count != 0)
        {
            foreach (var point in pointsLinkedWith)
            {
                LinesPoints currentLine = Array.Find(skillTreeLinkedTo.allLines.ToArray(), l => l.passivePoint1 == this && l.passivePoint2 == point ||
                                                                                                l.passivePoint1 == point && l.passivePoint2 == this);
                
                if (currentLine)
                {
                    //Debug.Log("line already Drawed");
                    currentLine.Points[0] = GetComponent<RectTransform>().localPosition;
                    RectTransform rectTransformPoint = point.GetComponent<RectTransform>();
                    currentLine.Points[1] = new Vector2(rectTransformPoint.localPosition.x, rectTransformPoint.localPosition.y);
                }
                else
                {
                    GameObject lineHandler = Instantiate(skillTreeLinkedTo.lineGameObject, canvas.transform);
                
                    LinesPoints line = lineHandler.GetComponent<LinesPoints>();
                    line.passivePoint1 = this;
                    line.passivePoint2 = point;
                    line.Points = new Vector2[2];
                
                    line.Points[0] = GetComponent<RectTransform>().localPosition;
                    RectTransform rectTransformPoint = point.GetComponent<RectTransform>();
                    line.Points[1] = new Vector2(rectTransformPoint.localPosition.x, rectTransformPoint.localPosition.y);
                    
                    line.transform.SetParent(skillTreeLinkedTo.lineContainer.transform);
                
                    skillTreeLinkedTo.allLines.Add(line);
                }
            }
        }
    }

    public void AskToGet()
    {
        //Debug.Log("Ask to get");
        if (CanGetPoint())
        {
            //Debug.Log("Get Passiv point");
            
            skillTreeLinkedTo._eachPointEffect += Effect;

            Effect(); 
            
            skillTreeLinkedTo.passivPointsLeft -= passivPointCost;
            passivPointSpentInThisPoint = passivPointCost;
            skillTreeLinkedTo.UpdatePassiveTextPoint();
            
            reached = true;
            
            SetLineColor();
        }
    }

    public void SetLineColor()
    {
        GetComponent<Image>().color = Color.yellow;
            
        foreach (var point in pointsLinkedWith)
        {
            LinesPoints linePoint = Array.Find(skillTreeLinkedTo.allLines.ToArray(), l =>
                l.passivePoint1 == this && l.passivePoint2 == point || l.passivePoint1 == point && l.passivePoint2 == this);
                
            if (point.reached)
            {
                linePoint.color = Color.yellow;
            }
            else
            {
                Color colorReachable = new Color(0.5f, 0.5f, 0);
                point.GetComponent<Image>().color = colorReachable;
                linePoint.color = colorReachable;
            }
        }
    }
    
    public virtual bool CanGetPoint()
    {
        if (reached)
        {
            //Debug.Log("you already have this point");
            return false;
        }

        CheckIfPointIsReachableWithPassing();

        if (!reachable)
        {
            Debug.Log("Point Not reachable");
            return false;
        }
        
        if (passivPointCost > skillTreeLinkedTo.passivPointsLeft)
        {
            Debug.Log("Not enough passive points");
            return false;
        }

        return true;
    }

    public void CheckIfPointIsReachableWithPassing()
    {
        if (Array.Find(pointsLinkedWith.ToArray(), point => point.reached))
        {
            reachable = true;
        }
        else
        {
            reachable = false;
        }
    }

    public void AskRemovePoint()
    {
        if (CanRemovePoint())
        {
            skillTreeLinkedTo._eachPointEffect -= Effect;
            
            skillTreeLinkedTo.UpdateSkillTreeValues(0);
            
            skillTreeLinkedTo.passivPointsLeft += passivPointSpentInThisPoint;
            skillTreeLinkedTo.UpdatePassiveTextPoint();
            
            reached = false;
            
            GetComponent<Image>().color = new Color(0.5f, 0.5f, 0);
            
            foreach (var point in pointsLinkedWith)
            {
                point.CheckIfPointIsReachableWithPassing();
                
                LinesPoints linePoint = Array.Find(skillTreeLinkedTo.allLines.ToArray(), l =>
                    l.passivePoint1 == this && l.passivePoint2 == point || l.passivePoint1 == point && l.passivePoint2 == this);
                
                if (point.reached)
                {
                    Color colorReachable = new Color(0.5f, 0.5f, 0);
                    linePoint.color = colorReachable;
                }
                else
                {
                    linePoint.color = Color.white;
                    
                    if (Array.Find(point.pointsLinkedWith.ToArray(), passivePoint => passivePoint.GetType() == typeof(StartPoint)) == null)
                    {
                        point.GetComponent<Image>().color = Color.white;
                    }
                }
            }
        }
    }

    public bool CanRemovePoint()
    {
        if (!reached)
        {
            Debug.Log("You don't have this point");
            return false;
        }

        /*foreach (var point2 in pointsLinkedWith)
        {
            if (point2.reached)
            {
                if (Array.Find(point2.pointsLinkedWith, point => point.reached && point != this) == null)
                {
                    return true;
                }
            }
        }*/

        foreach (var point2 in pointsLinkedWith)
        {
            if (point2.reached && point2.GetType() != typeof(StartPoint))
            {
                if (Array.FindAll(point2.pointsLinkedWith.ToArray(), point => point.reached).Length <= 1)
                {
                    Debug.Log("Points can't be removed while its the only link to another reached point");
                    return false;
                }
            }
        }

        return true;
    }

    public void SetToRemoveMode()
    {
        myButton?.onClick.RemoveAllListeners();
        myButton?.onClick.AddListener(AskRemovePoint);
    }
    
    public void SetToSpendMode()
    {
        myButton?.onClick.RemoveAllListeners();
        myButton?.onClick.AddListener(AskToGet);
    }
    
    public void OnHighlightEnter()
    {
        skillTreeLinkedTo.passivesPointsDescriptionTextDisplay.text = effectDescription;
        
        skillTreeLinkedTo.passivPointCostText.text = "Cost : " + passivPointCost + " Passiv Point";

        skillTreeLinkedTo.tipsContainer.SetActive(true);

        _highLighted = true;
    }

    public void OnHighlightExit()
    {
        skillTreeLinkedTo.tipsContainer.SetActive(false);
        
        _highLighted = false;
    }
    
    public virtual void Effect()
    {
        Debug.Log("Base Effect");
    }
}
