﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SkillTree : MonoBehaviour
{
     [Header("UI things")]
    
    public GameObject skillTree;

    public GameObject tipsContainer; //Set active true and change position on screen when a point is hightligthed
    
    public TextMeshProUGUI passivesPointsLeftDisplay;
    
    public TextMeshProUGUI passivesPointsDescriptionTextDisplay;
    public TextMeshProUGUI passivPointCostText;
    
    public float tipsPosOnScreenWidth;
    public float tipsPosOnScreenHeight;

    public GameObject lineContainer;

    public TextMeshProUGUI buttonSwitchModeText;
    
    [Header("Prefab")]
    
    public GameObject lineGameObject;

    [Header("Skill Tree")]

    public int passivPointsLeft;
    
    public bool useAllPoints;
    public int nbrOfActivePoints;

    public List<LinesPoints> allLines = new List<LinesPoints>();
    
    public delegate void EachPointEffect();

     public EachPointEffect _eachPointEffect;

     private bool _removePointMode;
     
     public delegate void SetToSpendMode();
     public event SetToSpendMode setToSpendMode;
     
     public delegate void SetToRemovedMode();
     public event SetToRemovedMode setToRemoveMode;

     void Start()
    {
        UpdatePassiveTextPoint();
    }
    
    void Update()
    {
        
    }

    public void UpdatePassiveTextPoint()
    {
        passivesPointsLeftDisplay.text = "Passive Point left : " + passivPointsLeft;
    }

    public void UpdateSkillTreeValues(int nbrOfActivePoints)
    {
        //set all Stats to 0

        if (useAllPoints)
        {
            if (_eachPointEffect != null)
            {
                _eachPointEffect();
            }
        }
        else
        {
            for (int i = 0; i < nbrOfActivePoints; i++)
            {
                var test = _eachPointEffect.GetInvocationList()[i];

                test.DynamicInvoke();
            }
        }
    }
    
    public void Switch_Remove_Spend_Points()
    {
        if (_removePointMode)
        {
            setToSpendMode();
            buttonSwitchModeText.text = "Remove Points";
        }
        else
        {
            setToRemoveMode();
            buttonSwitchModeText.text = "Spend Points";
        }

        _removePointMode = !_removePointMode;
    }

    public void AddSkill(string skill)
    {
        Debug.Log("AddSkill : " + skill);
    }

    public void AddStats(float f)
    {
        Debug.Log("AddStats : " + f);
    }

    public void MultiplyStat(float f)
    {
        
    }
}
